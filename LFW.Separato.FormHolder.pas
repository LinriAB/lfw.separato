unit LFW.Separato.FormHolder;

interface

uses
	LFW.Separato.Types,
	LFW.Separato.Interfaces,

	Forms;

function GetOrCreateContentForm( AFormClass : string; ACreateForm : boolean = false ) : IFormHolder;

implementation

uses
	RTTI,
	LFW.Misc;

type
	TContentForm = class( TInterfacedObject, IFormHolder )
	strict private
		FFormClassName : string;
		FFormClass : TFormClass;
		FForm : TForm;

		function _getFormClassName : string;
		function _getFormClass : TFormClass;
		function _getForm : TForm;
		procedure _setFormClassName( const AFormClassName : string );
		procedure _setFormClass( const AFormClass : TFormClass );
		procedure _setForm( const AForm : TForm );

	public
		procedure Verb( const AVerb : string ); overload;
		procedure Verb( const AVerb : string; AArguments : array of Variant ); overload;
		function Prop( const AProperty : string ) : TValue;

		property FormClassName : string read _getFormClassName write _setFormClassName;
		property FormClass : TFormClass read _getFormClass write _setFormClass;
		property Form : TForm read _getForm write _setForm;

	end;


function GetOrCreateContentForm( AFormClass : string; ACreateForm : boolean = false ) : IFormHolder;
var
	TheClass : TFormClass;
begin

	Result := TContentForm.Create;
	TheClass := TFormClass( FindAnyClass( AFormClass ) );
	Result.FormClass := TheClass;

	if ACreateForm then
		Result.Form;

end;


procedure TContentForm.Verb( const AVerb : string );
var
	Ctx : TRttiContext;
	Typ : TRttiType;
	Method : TRttiMethod;
	Args : array of TValue;
begin

	Typ := Ctx.GetType( FormClass );
	if Assigned( Typ ) then
	begin

		Method := Typ.GetMethod( AVerb );
		if Assigned( Method ) then
			Method.Invoke( Form, Args );

	end;

end;


function TContentForm.Prop( const AProperty : string ) : TValue;
var
	Ctx : TRttiContext;
	Typ : TRttiType;
	p : TRttiProperty;
begin

	Typ := Ctx.GetType( FormClass );
	if Assigned( Typ ) then
	begin

		p := Typ.GetProperty( AProperty );
		Result := p.GetValue( Form );

	end
	else
		Result := TValue.Empty;

end;


procedure TContentForm.Verb( const AVerb : string; AArguments : array of Variant );
var
	Ctx : TRttiContext;
	Typ : TRttiType;
	Method : TRttiMethod;
	Methods : TArray<TRttiMethod>;
	Parameters : TArray<TRttiParameter>;
	Args : array of TValue;
	i : integer;
begin

	Typ := Ctx.GetType( FormClass );
	if Assigned( Typ ) then
	begin

		Methods := Typ.GetMethods( AVerb );
		if Assigned( Methods ) then
		begin

			for Method in Methods do
			begin

				Parameters := Method.GetParameters;
				if Length( Parameters ) = Length( AArguments ) then
				begin

					SetLength( Args, Length( AArguments ) );
					for i := 0 to Length( AArguments ) - 1 do
						Args[ i ] := TValue.FromVariant( AArguments[ i ] );

					Method.Invoke( Form, Args );

					exit;

				end;

			end;

		end;

	end;

end;


function TContentForm._getForm : TForm;
begin

	if not Assigned( FForm ) then
		FForm := FFormClass.Create( Application.MainForm );

	Result := FForm;

end;


function TContentForm._getFormClass : TFormClass;
begin

	Result := FFormClass;

end;


function TContentForm._getFormClassName : string;
begin

	Result := FFormClassName;

end;


procedure TContentForm._setForm( const AForm : TForm );
begin

	FForm := AForm;

end;


procedure TContentForm._setFormClass( const AFormClass : TFormClass );
begin

	FFormClass := AFormClass;

end;


procedure TContentForm._setFormClassName( const AFormClassName : string );
begin

	FFormClassName := AFormClassName;

end;

end.
