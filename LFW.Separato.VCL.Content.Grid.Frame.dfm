inherited SeparatoVCLContentGridFrame: TSeparatoVCLContentGridFrame
  object grdData: TcxGrid
    Left = 0
    Top = 0
    Width = 663
    Height = 458
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 208
    ExplicitTop = 128
    ExplicitWidth = 250
    ExplicitHeight = 200
    object viewData: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsBehavior.IncSearch = True
      OptionsBehavior.FocusCellOnCycle = True
    end
    object lvlData: TcxGridLevel
      GridView = viewData
    end
  end
end
