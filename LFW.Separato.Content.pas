﻿unit LFW.Separato.Content;

interface

uses
	LFW.Separato,

{$IFDEF FMX}

	FMX.Types,

{$ENDIF}

	Forms, Controls;

type

{$IFDEF VCL}

	TSeparatoControl = TWinControl;

{$ENDIF}
{$IFDEF FMX}

	TSeparatoControl = TFmxObject;

{$ENDIF}

	TOnContentChanged = reference to procedure( const ANewContent, AOldContent : ISeparatoHolder );

	TLFWSeparatoContent = class helper for TForm
	strict private
	class var
		FCurrentContent : ISeparatoHolder;
		FCurrentContentParent : TSeparatoControl;
		FOnContentChanged : TOnContentChanged;

		function getCurrentFormHolder : ISeparatoHolder;
		function getCurrentContentParent : TSeparatoControl;
		function getOnContentChanged : TOnContentChanged;
		procedure setCurrentFormHolder( const AFormHolder : ISeparatoHolder );
		procedure setCurrentContentParent( const ACurrentContentParent : TSeparatoControl );
		procedure setOnContentChanged( const AOnContentChanged : TOnContentChanged );

	public
		property CurrentContent : ISeparatoHolder read getCurrentFormHolder write setCurrentFormHolder;
		property CurrentContentParent : TSeparatoControl read getCurrentContentParent write setCurrentContentParent;

		property OnContentChanged : TOnContentChanged read getOnContentChanged write setOnContentChanged;

	end;

implementation

function TLFWSeparatoContent.getCurrentFormHolder : ISeparatoHolder;
begin

	Result := FCurrentContent;

end;

function TLFWSeparatoContent.getCurrentContentParent : TSeparatoControl;
begin

	if Assigned( FCurrentContentParent ) then
		Result := FCurrentContentParent
	else
		Result := Self;

end;

function TLFWSeparatoContent.getOnContentChanged : TOnContentChanged;
begin

	Result := FOnContentChanged;

end;

procedure TLFWSeparatoContent.setCurrentFormHolder( const AFormHolder : ISeparatoHolder );
begin

	var newContent := AFormHolder;
	var oldContent := FCurrentContent;

	if AFormHolder = FCurrentContent then
		exit;

	if Assigned( FCurrentContent ) then
	begin

		FCurrentContent.Prop[ 'Visible' ] := false;
		FCurrentContent.Verb( VERB_LOOSE_FOCUS );
		FCurrentContent.Parent := nil;

	end;

	FCurrentContent := AFormHolder;

	if Assigned( FCurrentContent ) then
	begin

		FCurrentContent.Parent := CurrentContentParent;
		FCurrentContent.Prop[ 'Visible' ] := true;
		FCurrentContent.Verb( VERB_GET_FOCUS );

	end;

	if Assigned( FOnContentChanged ) then
		FOnContentChanged( newContent, oldContent );

end;

procedure TLFWSeparatoContent.setCurrentContentParent( const ACurrentContentParent : TSeparatoControl );
begin

	FCurrentContentParent := ACurrentContentParent;

end;

procedure TLFWSeparatoContent.setOnContentChanged( const AOnContentChanged : TOnContentChanged );
begin

	FOnContentChanged := AOnContentChanged;

end;

end.
