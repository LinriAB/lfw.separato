unit LFW.Separato.FrameFactory;

interface

uses
	SysUtils, Forms, Generics.Collections,

	LFW.Separato.Types,
	LFW.Separato.Interfaces,
	LFW.Separato.FrameHolder;

var
	FrameFactory : IFrameFactory;

implementation

type
	TFrameFactory = class( TInterfacedObject, IFrameFactory )
	strict private
		FFrames : TDictionary<string, IFrameHolder>;

		function _getFrame( AFrameClass : string ) : IFrameHolder;

	public
		constructor Create;
		destructor Destroy; override;

		procedure Register( const AFrameClassName : string ); overload;
		procedure Register( const AFrameClass : TFrameClass ); overload;

	end;

constructor TFrameFactory.Create;
begin

	FFrames := TDictionary<string, IFrameHolder>.Create;

end;

destructor TFrameFactory.Destroy;
begin

	FreeAndNil( FFrames );

	inherited;

end;

function TFrameFactory._getFrame( AFrameClass : string ) : IFrameHolder;
begin

	if not FFrames.TryGetValue( AFrameClass, Result ) then
		raise Exception.Create( 'FrameClass not found' );

end;

procedure TFrameFactory.Register( const AFrameClassName : string );
var
	ContentFrame : IFrameHolder;
begin

	ContentFrame := GetOrCreateContentFrame( AFrameClassName );
	FFrames.AddOrSetValue( AFrameClassName, ContentFrame );

end;

procedure TFrameFactory.Register( const AFrameClass : TFrameClass );
begin

	Register( AFrameClass.ClassName );

end;

initialization

FrameFactory := TFrameFactory.Create;

end.
