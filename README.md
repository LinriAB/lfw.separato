# LFW.Separato

The LFW.Separato framework is made free to use for anyone in whatever context one wishes. I ask 
only that the below signature be included in any work descending from this code and that, if you 
find it useful, you'll follow and promote my Clean Coding initiative in whatever way you find 
most beneficial.

Sincerely

Rickard Engberg
Coding Cleaner
www.codingcleaner.com
https://www.facebook.com/CodingCleaner/
rickard@codingcleaner.com