unit LFW.Separato.FrameHolder;

interface

uses
	LFW.Separato.Types,
	LFW.Separato.Interfaces,

	Forms;

function GetOrCreateContentFrame( AFrameClass : string; ACreateFrame : boolean = false ) : IFrameHolder;

implementation

uses
	RTTI,
	LFW.Misc;

type
	TContentFrame = class( TInterfacedObject, IFrameHolder )
	strict private
		FFrameClassName : string;
		FFrameClass : TFrameClass;
		FFrame : TFrame;

		function _getFrameClassName : string;
		function _getFrameClass : TFrameClass;
		function _getFrame : TFrame;
		procedure _setFrameClassName( const AFrameClassName : string );
		procedure _setFrameClass( const AFrameClass : TFrameClass );
		procedure _setFrame( const AFrame : TFrame );

	public
		procedure Verb( const AVerb : string ); overload;
		procedure Verb( const AVerb : string; AArguments : array of Variant ); overload;
		function Prop( const AProperty : string ) : TValue;

		property FrameClassName : string read _getFrameClassName write _setFrameClassName;
		property FrameClass : TFrameClass read _getFrameClass write _setFrameClass;
		property Frame : TFrame read _getFrame write _setFrame;

	end;


function GetOrCreateContentFrame( AFrameClass : string; ACreateFrame : boolean = false ) : IFrameHolder;
var
	TheClass : TFrameClass;
begin

	Result := TContentFrame.Create;
	TheClass := TFrameClass( FindAnyClass( AFrameClass ) );
	Result.FrameClass := TheClass;

	if ACreateFrame then
		Result.Frame;

end;


function TContentFrame._getFrame : TFrame;
begin

	if not Assigned( FFrame ) then
		FFrame := FFrameClass.Create( Application.MainForm );

	Result := FFrame;

end;


function TContentFrame._getFrameClass : TFrameClass;
begin

	Result := FFrameClass;

end;


function TContentFrame._getFrameClassName : string;
begin

	Result := FFrameClassName;

end;


procedure TContentFrame._setFrame( const AFrame : TFrame );
begin

	FFrame := AFrame;

end;


procedure TContentFrame._setFrameClass( const AFrameClass : TFrameClass );
begin

	FFrameClass := AFrameClass;

end;


procedure TContentFrame._setFrameClassName( const AFrameClassName : string );
begin

	FFrameClassName := AFrameClassName;

end;


procedure TContentFrame.Verb( const AVerb : string );
var
	Ctx : TRttiContext;
	Typ : TRttiType;
	Method : TRttiMethod;
	Args : array of TValue;
begin

	Typ := Ctx.GetType( FrameClass );
	if Assigned( Typ ) then
	begin

		Method := Typ.GetMethod( AVerb );
		if Assigned( Method ) then
			Method.Invoke( Frame, Args );

	end;

end;


function TContentFrame.Prop( const AProperty : string ) : TValue;
var
	Ctx : TRttiContext;
	Typ : TRttiType;
	p : TRttiProperty;
begin

	Typ := Ctx.GetType( FrameClass );
	if Assigned( Typ ) then
	begin

		p := Typ.GetProperty( AProperty );
		Result := p.GetValue( Frame );

	end
	else
		Result := TValue.Empty;

end;


procedure TContentFrame.Verb( const AVerb : string; AArguments : array of Variant );
var
	Ctx : TRttiContext;
	Typ : TRttiType;
	Method : TRttiMethod;
	Methods : TArray<TRttiMethod>;
	Parameters : TArray<TRttiParameter>;
	Args : array of TValue;
	i : integer;
begin

	Typ := Ctx.GetType( FrameClass );
	if Assigned( Typ ) then
	begin

		Methods := Typ.GetMethods( AVerb );
		if Assigned( Methods ) then
		begin

			for Method in Methods do
			begin

				Parameters := Method.GetParameters;
				if Length( Parameters ) = Length( AArguments ) then
				begin

					SetLength( Args, Length( AArguments ) );
					for i := 0 to Length( AArguments ) - 1 do
						Args[ i ] := TValue.FromVariant( AArguments[ i ] );

					Method.Invoke( Frame, Args );

					exit;

				end;

			end;

		end;

	end;

end;

end.
