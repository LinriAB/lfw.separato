﻿unit LFW.Separato.VCL.Content.Frame;

interface

uses
	LFW.Separato.Interfaces,

	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
	VCL.Graphics, VCL.Controls, VCL.Forms, VCL.Dialogs;

type
	TSeparatoVCLContentFrame = class( TFrame, ISeparatoContent )
	strict protected
		FUpdateCount : integer;

		procedure doAfterConstruction; virtual;
		procedure doBeforeDestruction; virtual;
		procedure doBeginUpdate; virtual;
		procedure doEndUpdate; virtual;
		procedure doGetFocus; virtual;
		procedure doLooseFocus; virtual;
		procedure doRefresh; virtual;
		procedure doAdd; virtual;
		procedure doEdit; virtual;

	public
		procedure AfterConstruction; override;
		procedure BeforeDestruction; override;

		procedure BeginUpdate;
		procedure EndUpdate;
		procedure GetFocus;
		procedure LooseFocus;
		procedure Refresh;
		function IsUpdating : boolean;

		procedure Add;
		procedure Edit;

	end;

implementation

{$R *.dfm}

procedure TSeparatoVCLContentFrame.doAfterConstruction;
begin

end;

procedure TSeparatoVCLContentFrame.doBeforeDestruction;
begin

end;

procedure TSeparatoVCLContentFrame.doBeginUpdate;
begin

	inc( FUpdateCount );

end;

procedure TSeparatoVCLContentFrame.doEndUpdate;
begin

	if FUpdateCount > 0 then
	begin

		dec( FUpdateCount );
		if not IsUpdating then
			doRefresh;

	end;

end;

procedure TSeparatoVCLContentFrame.doGetFocus;
begin

	doRefresh;

end;

procedure TSeparatoVCLContentFrame.doLooseFocus;
begin

end;

procedure TSeparatoVCLContentFrame.doRefresh;
begin

end;

procedure TSeparatoVCLContentFrame.doAdd;
begin

end;

procedure TSeparatoVCLContentFrame.doEdit;
begin

end;

procedure TSeparatoVCLContentFrame.AfterConstruction;
begin

	inherited;

	doAfterConstruction;

end;

procedure TSeparatoVCLContentFrame.BeforeDestruction;
begin

	doBeforeDestruction;

	inherited;

end;

procedure TSeparatoVCLContentFrame.BeginUpdate;
begin

	doBeginUpdate;

end;

procedure TSeparatoVCLContentFrame.EndUpdate;
begin

	doEndUpdate;

end;

procedure TSeparatoVCLContentFrame.GetFocus;
begin

	doGetFocus;

end;

procedure TSeparatoVCLContentFrame.LooseFocus;
begin

	doLooseFocus;

end;

procedure TSeparatoVCLContentFrame.Refresh;
begin

	if IsUpdating then
		exit;

	doRefresh;

end;

function TSeparatoVCLContentFrame.IsUpdating : boolean;
begin

	Result := ( FUpdateCount > 0 );

end;

procedure TSeparatoVCLContentFrame.Add;
begin

	doAdd;

end;

procedure TSeparatoVCLContentFrame.Edit;
begin

	doEdit;

end;

end.
