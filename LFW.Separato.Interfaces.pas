unit LFW.Separato.Interfaces;

interface

uses
	LFW.Separato.Types,

	Forms, RTTI;

type
	IVerbResult = interface
	end;

	ISeparatoContent = interface

		procedure doAfterConstruction;
		procedure doBeforeDestruction;
		procedure doGetFocus;
		procedure doLooseFocus;
		procedure doRefresh;
		procedure doAdd;
		procedure doEdit;

		procedure BeginUpdate;
		procedure EndUpdate;
		procedure GetFocus;
		procedure LooseFocus;
		procedure Refresh;

		procedure Add;
		procedure Edit;

	end;

	IFrameHolder = interface
		function _getFrameClassName : string;
		function _getFrameClass : TFrameClass;
		function _getFrame : TFrame;
		procedure _setFrameClassName( const AFrame : string );
		procedure _setFrameClass( const AFrameClass : TFrameClass );
		procedure _setFrame( const AFrame : TFrame );

		procedure Verb( const AVerb : string ); overload;
		procedure Verb( const AVerb : string; AArguments : array of Variant ); overload;
		function Prop( const AProperty : string ) : TValue;

		property FrameClassName : string read _getFrameClassName write _setFrameClassName;
		property FrameClass : TFrameClass read _getFrameClass write _setFrameClass;
		property Frame : TFrame read _getFrame write _setFrame;

	end;

	IFormHolder = interface
		function _getFormClassName : string;
		function _getFormClass : TFormClass;
		function _getForm : TForm;
		procedure _setFormClassName( const AForm : string );
		procedure _setFormClass( const AFormClass : TFormClass );
		procedure _setForm( const AForm : TForm );

		procedure Verb( const AVerb : string ); overload;
		procedure Verb( const AVerb : string; AArguments : array of Variant ); overload;
		function Prop( const AProperty : string ) : TValue;

		property FormClassName : string read _getFormClassName write _setFormClassName;
		property FormClass : TFormClass read _getFormClass write _setFormClass;
		property Form : TForm read _getForm write _setForm;

	end;

	IFrameFactory = interface
		function _getFrame( AFrameClassName : string ) : IFrameHolder;

		procedure Register( const AFrameClassName : string ); overload;
		procedure Register( const AFrameClass : TFrameClass ); overload;

		property Frames[ AFrameClassName : string ] : IFrameHolder read _getFrame;

	end;

	IFormFactory = interface
		function _getForm( AFormClassName : string ) : IFormHolder;

		procedure Register( const AFormClassName : string ); overload;
		procedure Register( const AFormClass : TFormClass ); overload;

		property Forms[ AFormClassName : string ] : IFormHolder read _getForm;

	end;

implementation

end.
