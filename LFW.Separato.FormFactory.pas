unit LFW.Separato.FormFactory;

interface

uses
	SysUtils, Forms, Generics.Collections,

	LFW.Separato.Interfaces,
	LFW.Separato.FormHolder;

var
	FormFactory : IFormFactory;

implementation

type
	TFormFactory = class( TInterfacedObject, IFormFactory )
	strict private
		FForms : TDictionary<string, IFormHolder>;

		function _getForm( AFormClass : string ) : IFormHolder;

	public
		constructor Create;
		destructor Destroy; override;

		procedure Register( const AFormClassName : string ); overload;
		procedure Register( const AFormClass : TFormClass ); overload;

	end;


constructor TFormFactory.Create;
begin

	FForms := TDictionary<string, IFormHolder>.Create;

end;


destructor TFormFactory.Destroy;
begin

	FreeAndNil( FForms );

	inherited;

end;


function TFormFactory._getForm( AFormClass : string ) : IFormHolder;
begin

	if not FForms.TryGetValue( AFormClass, Result ) then
		raise Exception.Create( 'FormClass not found' );

end;


procedure TFormFactory.Register( const AFormClassName : string );
var
	ContentForm : IFormHolder;
begin

	ContentForm := GetOrCreateContentForm( AFormClassName );
	FForms.AddOrSetValue( AFormClassName, ContentForm );

end;


procedure TFormFactory.Register( const AFormClass : TFormClass );
begin

	Register( AFormClass.ClassName );

end;

initialization

FormFactory := TFormFactory.Create;

end.
