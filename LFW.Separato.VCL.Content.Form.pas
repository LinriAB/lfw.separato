unit LFW.Separato.VCL.Content.Form;

interface

uses
	LFW.Separato.Interfaces,

	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, VCL.Graphics,
	VCL.Controls, VCL.Forms, VCL.Dialogs;

type
	TSeparatoVCLContentForm = class( TForm, ISeparatoContent )
	strict protected
		FUpdateCount : integer;

		procedure doAfterConstruction; virtual;
		procedure doBeforeDestruction; virtual;
		procedure doBeginUpdate; virtual;
		procedure doEndUpdate; virtual;
		procedure doGetFocus; virtual;
		procedure doLooseFocus; virtual;
		procedure doRefresh; virtual;
		procedure doAdd; virtual;
		procedure doEdit; virtual;

	public
		procedure AfterConstruction; override;
		procedure BeforeDestruction; override;

		procedure BeginUpdate;
		procedure EndUpdate;
		procedure GetFocus;
		procedure LooseFocus;
		procedure Refresh;
		function IsUpdating : boolean;

		procedure Add;
		procedure Edit;

	end;

implementation

{$R *.dfm}

procedure TSeparatoVCLContentForm.doAfterConstruction;
begin

end;

procedure TSeparatoVCLContentForm.doBeforeDestruction;
begin

end;

procedure TSeparatoVCLContentForm.doBeginUpdate;
begin

	inc( FUpdateCount );

end;

procedure TSeparatoVCLContentForm.doGetFocus;
begin

	doRefresh;

end;

procedure TSeparatoVCLContentForm.doLooseFocus;
begin

end;

procedure TSeparatoVCLContentForm.doRefresh;
begin

end;

procedure TSeparatoVCLContentForm.doAdd;
begin

end;

procedure TSeparatoVCLContentForm.doEdit;
begin

end;

procedure TSeparatoVCLContentForm.doEndUpdate;
begin

	if FUpdateCount > 0 then
	begin

		dec( FUpdateCount );
		if not IsUpdating then
			doRefresh;

	end;

end;

procedure TSeparatoVCLContentForm.AfterConstruction;
begin

	inherited;

	doAfterConstruction;

end;

procedure TSeparatoVCLContentForm.BeforeDestruction;
begin

	doBeforeDestruction;

	inherited;

end;

procedure TSeparatoVCLContentForm.BeginUpdate;
begin

	doBeginUpdate;

end;

procedure TSeparatoVCLContentForm.GetFocus;
begin

	doGetFocus;

end;

function TSeparatoVCLContentForm.IsUpdating : boolean;
begin

	Result := ( FUpdateCount > 0 );

end;

procedure TSeparatoVCLContentForm.LooseFocus;
begin

	doLooseFocus;

end;

procedure TSeparatoVCLContentForm.Refresh;
begin

	if IsUpdating then
		exit;

	doRefresh;

end;

procedure TSeparatoVCLContentForm.Add;
begin

	doAdd;

end;

procedure TSeparatoVCLContentForm.Edit;
begin

	doEdit;

end;

procedure TSeparatoVCLContentForm.EndUpdate;
begin

	doEndUpdate;

end;

end.
