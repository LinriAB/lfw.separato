﻿unit LFW.Separato.FMX.Content.Frame;

interface

uses
	LFW.Separato.Interfaces,

	System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
	FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs;

type
	TSeparatoFMXContentFrame = class( TFrame, ISeparatoContent )
	strict protected
		procedure doAfterConstruction; virtual;
		procedure doBeforeDestruction; virtual;
		procedure doGetFocus; virtual;
		procedure doLooseFocus; virtual;
		procedure doRefresh; virtual;
		procedure doAdd; virtual;
		procedure doEdit; virtual;

	public
		procedure AfterConstruction; override;
		procedure BeforeDestruction; override;

		procedure BeginUpdate;
		procedure EndUpdate;
		procedure GetFocus;
		procedure LooseFocus;
		procedure Refresh;

		procedure Add;
		procedure Edit;

	end;

implementation

{$R *.fmx}

procedure TSeparatoFMXContentFrame.doAfterConstruction;
begin

end;

procedure TSeparatoFMXContentFrame.doBeforeDestruction;
begin

end;

procedure TSeparatoFMXContentFrame.doGetFocus;
begin

	doRefresh;

end;

procedure TSeparatoFMXContentFrame.doLooseFocus;
begin

end;

procedure TSeparatoFMXContentFrame.doRefresh;
begin

end;

procedure TSeparatoFMXContentFrame.doAdd;
begin

end;

procedure TSeparatoFMXContentFrame.doEdit;
begin

end;

procedure TSeparatoFMXContentFrame.AfterConstruction;
begin

	inherited;

	doAfterConstruction;

end;

procedure TSeparatoFMXContentFrame.BeforeDestruction;
begin

	doBeforeDestruction;

	inherited;

end;

procedure TSeparatoFMXContentFrame.BeginUpdate;
begin

	doBeginUpdate;

end;

procedure TSeparatoFMXContentFrame.EndUpdate;
begin

	doEndUpdate;
	if not IsUpdating then
    doRefresh;

end;

procedure TSeparatoFMXContentFrame.GetFocus;
begin

	doGetFocus;

end;

procedure TSeparatoFMXContentFrame.LooseFocus;
begin

	doLooseFocus;

end;

procedure TSeparatoFMXContentFrame.Refresh;
begin

	if IsUpdating then
		exit;

	doRefresh;

end;

procedure TSeparatoFMXContentFrame.Add;
begin

	doAdd;

end;

procedure TSeparatoFMXContentFrame.Edit;
begin

	doEdit;

end;

end.
