unit LFW.Separato;

interface

uses

{$IFDEF FMX}

	FMX.Types,

{$ENDIF}

	Forms, Controls, Classes, SysUtils, Generics.Collections, Rtti, UITypes;

type

{$IFDEF VCL}

	TSeparatoControl = TComponent; // TWinControl;

{$ENDIF}
{$IFDEF FMX}

	TSeparatoControl = TFmxObject;

{$ENDIF}

	TSeparatoControlClass = class of TSeparatoControl;

	TVerb = class( TCustomAttribute )

	end;

	TModalCallbackFrame = reference to procedure( ASender : TFrame; AModalResult : TModalResult );
	TModalCallbackForm = reference to procedure( ASender : TForm; AModalResult : TModalResult );

	ISeparatoHolder = interface
		function _getHolderName : string;
		function _getAsObject : TSeparatoControl;
		function _getParent : TSeparatoControl;
		function _getProperty( const APropertyName : string ) : TValue;
		function _getReuse : boolean;
		procedure _setParent( AParent : TSeparatoControl );
		procedure _setProperty( const APropertyName : string; AValue : TValue );
		procedure _setReuse( AReuse : boolean );

		procedure InstantiateObject( AReuse : boolean = true );
		procedure UnInstantiateObject;
		procedure ClearOwnedObject;

		function SetClassType( AClassType : TSeparatoControlClass ) : ISeparatoHolder;
		function SetObjectInstance( AObjectInstance : TSeparatoControl ) : ISeparatoHolder;
		function SetParent( AParent : TSeparatoControl ) : ISeparatoHolder;
		function SetReuse( AReuse : boolean ) : ISeparatoHolder;

		function Verb( const AVerb : string; AArguments : array of TValue ) : TValue; overload;
		function Verb( const AVerb : string ) : TValue; overload;

		property Name : string read _getHolderName;
		property Parent : TSeparatoControl read _getParent write _setParent;
		property AsObject : TSeparatoControl read _getAsObject;
		property Prop[ const APropertyName : string ] : TValue read _getProperty write _setProperty;
		property Reuse : boolean read _getReuse write _setReuse;

	end;

	TSeparatoClassList = TDictionary<string, ISeparatoHolder>;

	TSeparato = class
	strict private
		FClassList : TSeparatoClassList;

		function _getHolder( const AObjectClassName : string ) : ISeparatoHolder; overload;
		function _getProperty( const AObjectClassName, APropertyName : string ) : TValue;
		procedure _setProperty( const AObjectClassName, APropertyName : string; const AValue : TValue );

	public
		constructor Create;
		destructor Destroy; override;

		procedure Register( const AObjectClass : TSeparatoControlClass; AReuse : boolean = false ); overload;
		procedure Register( const AObjectClass : TSeparatoControlClass; const AForm : TSeparatoControl );
			overload;

		function AsType<T>( const AObjectClassName : string; const AReuse : boolean = true;
			const ASetOwnerTo : TSeparatoControl = nil ) : T;

		function Verb<T>( const AObjectClassName, AVerb : string; AArguments : array of TValue ) : T; overload;
		function Verb<T>( const AObjectClassName, AVerb : string ) : T; overload;
		procedure Verb( const AObjectClassName, AVerb : string; AArguments : array of TValue ); overload;
		procedure Verb( const AObjectClassName, AVerb : string ); overload;
		procedure ShowModal( const AObjectClassName : string; const ACallBack : TModalCallbackForm );

		property Prop[ const AObjectClassName, APropertyName : string ] : TValue read _getProperty
			write _setProperty;
		property Holder[ const AHolderName : string ] : ISeparatoHolder read _getHolder; default;

	end;

const

{$IFDEF MSWINDOWS}

	VERB_SHOW_MODAL = 'ShowModal';

{$ENDIF}

	VERB_SHOW = 'Show';
	VERB_GET_FOCUS = 'GetFocus';
	VERB_LOOSE_FOCUS = 'LooseFocus';
	VERB_ADD = 'Add';
	VERB_EDIT = 'Edit';
	VERB_DELETE = 'Delete';
	VERB_REFRESH = 'Refresh';

var
	Separato : TSeparato;

implementation

uses
	LFW.Types.Helpers;

resourcestring
	strPropertyNotFound = 'Property %s not found';

type
	TFreeNotifier = class( TComponent )
	strict private
		FSeparatoHolder : ISeparatoHolder;

	public
		constructor Create( AObject : TComponent; AHolder : ISeparatoHolder ); reintroduce;
		destructor Destroy; override;

	end;

	TSeparatoHolder = class( TInterfacedObject, ISeparatoHolder )
	strict private
		FObject : TSeparatoControl;
		FObjectClass : TSeparatoControlClass;
		FReuse : boolean;

		function _getHolderName : string;
		function _getAsObject : TSeparatoControl;
		function _getParent : TSeparatoControl;
		function _getReuse : boolean;
		procedure _setParent( AParent : TSeparatoControl );
		procedure _setReuse( AReuse : boolean );

	strict private
		function _getProperty( const APropertyName : string ) : TValue;
		procedure _setProperty( const APropertyName : string; AValue : TValue );

		procedure InstantiateObject( AReuse : boolean = true );

	public
		constructor Create;
		destructor Destroy; override;

		procedure UnInstantiateObject;
		procedure ClearOwnedObject;

		function SetClassType( AClassType : TSeparatoControlClass ) : ISeparatoHolder;
		function SetObjectInstance( AObjectInstance : TSeparatoControl ) : ISeparatoHolder;
		function SetParent( AParent : TSeparatoControl ) : ISeparatoHolder;
		function SetReuse( AReuse : boolean ) : ISeparatoHolder;

		function Verb( const AVerb : string; AArguments : array of TValue ) : TValue; overload;
		function Verb( const AVerb : string ) : TValue; overload;

		property Name : string read _getHolderName;
		property Parent : TSeparatoControl read _getParent write _setParent;
		property AsObject : TSeparatoControl read _getAsObject;
		property Prop[ const APropertyName : string ] : TValue read _getProperty write _setProperty;
		property Reuse : boolean read _getReuse write _setReuse;

	end;

function TSeparato._getHolder( const AObjectClassName : string ) : ISeparatoHolder;
var
	Holder : ISeparatoHolder;
begin

	if not FClassList.TryGetValue( AObjectClassName, Holder ) then
		raise Exception.CreateFmt( 'Class %s not found', [ AObjectClassName ] );

	Holder.InstantiateObject( Holder.Reuse );
	Result := Holder;

end;

function TSeparato._getProperty( const AObjectClassName, APropertyName : string ) : TValue;
begin

	Result := Holder[ AObjectClassName ].Prop[ APropertyName ];

end;

procedure TSeparato._setProperty( const AObjectClassName, APropertyName : string; const AValue : TValue );
begin

	Holder[ AObjectClassName ].Prop[ APropertyName ] := AValue;

end;

constructor TSeparato.Create;
begin

	inherited;

	FClassList := TSeparatoClassList.Create;

end;

destructor TSeparato.Destroy;
begin

	FreeAndNil( FClassList );

	inherited;

end;

procedure TSeparato.Register( const AObjectClass : TSeparatoControlClass; AReuse : boolean = false );
begin

	FClassList.Add(
		AObjectClass.ClassName,
		TSeparatoHolder.Create
			.SetClassType( AObjectClass )
			.SetReuse( AReuse )
		);

end;

procedure TSeparato.Register( const AObjectClass : TSeparatoControlClass; const AForm : TSeparatoControl );
begin

	FClassList.Add(
		AObjectClass.ClassName,
		TSeparatoHolder.Create
			.SetClassType( AObjectClass )
			.SetObjectInstance( AForm )
			.SetReuse( true )
		);

end;

procedure TSeparato.ShowModal( const AObjectClassName : string; const ACallBack : TModalCallbackForm );
begin

	var
	f := AsType<TForm>( AObjectClassName );
	if Assigned( f ) then
	begin

		if not ( f is TForm ) then
			raise Exception.CreateFmt( 'Object %s not a TFOrm object', [ AObjectClassName ] );

		var
		modalResult := f.ShowModal;
		if Assigned( ACallBack ) then
		begin

			ACallBack( f, modalResult );

		end;

	end
	else
		raise Exception.CreateFmt( 'Object %s not found', [ AObjectClassName ] );

end;

function TSeparato.AsType<T>( const AObjectClassName : string; const AReuse : boolean = true;
	const ASetOwnerTo : TSeparatoControl = nil ) : T;
var
	h : ISeparatoHolder;
	o : TSeparatoControl absolute Result;
begin

	h := Holder[ AObjectClassName ];
	if Assigned( ASetOwnerTo ) then
		h.Parent := ASetOwnerTo;
	o := h.AsObject;

end;

function TSeparato.Verb<T>( const AObjectClassName, AVerb : string; AArguments : array of TValue ) : T;
begin

	Result := Holder[ AObjectClassName ].Verb( AVerb, AArguments ).AsType<T>;

end;

function TSeparato.Verb<T>( const AObjectClassName, AVerb : string ) : T;
begin

	Result := Verb<T>( AObjectClassName, AVerb, [ ] );

end;

procedure TSeparato.Verb( const AObjectClassName, AVerb : string; AArguments : array of TValue );
begin

	Holder[ AObjectClassName ].Verb( AVerb, AArguments );

end;

procedure TSeparato.Verb( const AObjectClassName, AVerb : string );
begin

	Holder[ AObjectClassName ].Verb( AVerb );

end;

// function TSeparato.Holder( const AObjectClassName : string; AReuse : boolean = true ) : ISeparatoHolder;
// begin
//
// Result := _getHolder( AObjectClassName, AReuse );
//
// end;

{ - TSeparatoHolder ---------------------------------------------------------- }

procedure TSeparatoHolder.ClearOwnedObject;
begin

	FObject := nil;

end;

constructor TSeparatoHolder.Create;
begin

	FObject := nil;
	FObjectClass := nil;
	FReuse := false;

end;

destructor TSeparatoHolder.Destroy;
begin

	// TODO :	FreeAndNil( FObject );

	inherited;

end;

function TSeparatoHolder._getAsObject : TSeparatoControl;
begin

	if not Assigned( FObject ) then
		InstantiateObject;

	Result := FObject;

end;

function TSeparatoHolder._getHolderName : string;
begin

	Result := FObjectClass.ClassName;

end;

function TSeparatoHolder._getParent : TSeparatoControl;
begin

{$IFDEF VCL}

	if FObject is TWinControl then
		Result := TWinControl( FObject ).Parent
	else
		Result := nil;

{$ELSE}

	Result := TSeparatoControl( FObject ).Parent;

{$ENDIF}

end;

function TSeparatoHolder._getProperty( const APropertyName : string ) : TValue;
var
	Ctx : TRttiContext;
	Typ : TRttiType;
	p : TRttiProperty;
begin

	Typ := Ctx.GetType( FObjectClass );
	if Assigned( Typ ) then
	begin

		p := Typ.GetProperty( APropertyName );
		Result := p.GetValue( FObject );

	end
	else
		Result := TValue.Empty;

end;

function TSeparatoHolder._getReuse : boolean;
begin

	Result := FReuse;

end;

procedure TSeparatoHolder._setParent( AParent : TSeparatoControl );
begin

{$IFDEF VCL}

	if Assigned( FObject ) and ( FObject is TWinControl ) then
		TWinControl( FObject ).Parent := TWinControl( AParent );

{$ELSE}

	TSeparatoControl( FObject ).Parent := TSeparatoControl( AParent );

{$ENDIF}

end;

procedure TSeparatoHolder._setProperty( const APropertyName : string; AValue : TValue );
var
	Ctx : TRttiContext;
	Typ : TRttiType;
	p : TRttiProperty;
begin

	Typ := Ctx.GetType( FObjectClass );
	if Assigned( Typ ) then
	begin

		p := Typ.GetProperty( APropertyName );
		if Assigned( p ) then
			p.SetValue( FObject, AValue )
		else
			raise Exception.CreateFmt( strPropertyNotFound, [ APropertyName ] );

	end;

end;

procedure TSeparatoHolder._setReuse( AReuse : boolean );
begin

	FReuse := AReuse

end;

procedure TSeparatoHolder.InstantiateObject( AReuse : boolean = true );
begin

	if AReuse then
	begin

		if not Assigned( FObject ) then
		begin

			FObject := FObjectClass.Create( Application );
			FObject.Name := TGUID.NewGuid.AsControlName;
			TFreeNotifier.Create( FObject, Self );

		end;

	end
	else
	begin

		FObject := FObjectClass.Create( Application );
		FObject.Name := TGUID.NewGuid.AsControlName;
		TFreeNotifier.Create( FObject, Self );

	end;

end;

function TSeparatoHolder.SetClassType( AClassType : TSeparatoControlClass ) : ISeparatoHolder;
begin

	FObjectClass := AClassType;

	Result := Self;

end;

function TSeparatoHolder.SetObjectInstance( AObjectInstance : TSeparatoControl ) : ISeparatoHolder;
begin

	FObject := AObjectInstance;

	Result := Self;

end;

function TSeparatoHolder.SetParent( AParent : TSeparatoControl ) : ISeparatoHolder;
begin

{$IFDEF VCL}

	if not( AParent is TWinControl ) then
		raise Exception.Create( 'Control class is not TWinControl' );

	TWinControl( FObject ).Parent := TWinControl( AParent );

{$ELSE}

	TSeparatoControl( FObject ).Parent := TSeparatoControl( AParent );

{$ENDIF}

	Result := Self;

end;

function TSeparatoHolder.SetReuse( AReuse : boolean ) : ISeparatoHolder;
begin

	Result := Self;
	FReuse := AReuse;

end;

procedure TSeparatoHolder.UnInstantiateObject;
begin

	FreeAndNil( FObject );

end;

function TSeparatoHolder.Verb( const AVerb : string; AArguments : array of TValue ) : TValue;
var
	Ctx : TRttiContext;
	Typ : TRttiType;
	Method : TRttiMethod;
	Parameters : TArray<TRttiParameter>;
	Args : array of TValue;
begin

	Typ := Ctx.GetType( FObjectClass );
	if Assigned( Typ ) then
	begin

		for Method in Typ.GetMethods( AVerb ) do
		begin

			Parameters := Method.GetParameters;

			if Length( Parameters ) = Length( AArguments ) then
			begin

				SetLength( Args, Length( AArguments ) );
				for var i := 0 to Length( AArguments ) - 1 do
					Args[ i ] := AArguments[ i ];

				Result := Method.Invoke( FObject, Args );

				exit;

			end;

		end;

	end;

end;

function TSeparatoHolder.Verb( const AVerb : string ) : TValue;
begin

	Result := Verb( AVerb, [ ] );

end;

(*
	function TSeparatoHolder.Prop( const AProperty : string ) : TValue;
	var
	Ctx : TRttiContext;
	Typ : TRttiType;
	p : TRttiProperty;
	begin

	Typ := Ctx.GetType( FObjectClass );
	if Assigned( Typ ) then
	begin

	p := Typ.GetProperty( AProperty );
	if not Assigned( p ) then
	raise Exception.CreateFmt( 'Can''t find property %s of instance %s', [ AProperty, FObjectClass.ClassName ] );

	Result := p.GetValue( FObject );

	end
	else
	raise Exception.CreateFmt( 'Can''t find instance %s', [ FObjectClass.ClassName ] );

	end;
*)

{ TFreeNotifier }

constructor TFreeNotifier.Create( AObject : TComponent; AHolder : ISeparatoHolder );
begin

	inherited Create( AObject );

	FSeparatoHolder := AHolder;

end;

destructor TFreeNotifier.Destroy;
begin

	if Assigned( FSeparatoHolder ) then
		TSeparatoHolder( FSeparatoHolder ).ClearOwnedObject;

	inherited;

end;

initialization

Separato := TSeparato.Create;

finalization

FreeAndNil( Separato );

end.
